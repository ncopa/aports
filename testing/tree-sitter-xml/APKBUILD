# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=tree-sitter-xml
pkgver=0.6.0
pkgrel=0
pkgdesc="XML & DTD grammars for tree-sitter"
url="https://github.com/ObserverOfTime/tree-sitter-xml"
arch="all"
license="MIT"
makedepends="tree-sitter-dev"
subpackages="$pkgname-doc"
provides="tree-sitter-dtd=$pkgver-r$pkgrel"
install_if="tree-sitter-grammars"
source="$pkgname-$pkgver.tar.gz::https://github.com/ObserverOfTime/tree-sitter-xml/archive/refs/tags/v$pkgver.tar.gz"
options="!check"  # no tests for shared lib

_langs='xml dtd'

build() {
	local lang; for lang in $_langs; do
		abuild-tree-sitter build -s "$lang/src"
	done
}

package() {
	local lang; for lang in $_langs; do
		DESTDIR="$pkgdir" abuild-tree-sitter install -s "$lang/src" -q "$lang/queries"
	done
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
a39e3cb9b78a9032a0787c439247332ebeae3170e41e9aa0db3d3381fd5fd58d95659a667538884ceffb72a2a0f336efc8ef43830b2d6f7753aaa4293669fec8  tree-sitter-xml-0.6.0.tar.gz
"
